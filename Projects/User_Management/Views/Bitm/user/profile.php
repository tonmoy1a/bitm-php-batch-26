<?php
include '../../../vendor/autoload.php';
use RegistrationApp\Bitm\user_login\UserLogin;
use RegistrationApp\Bitm\user_profile\UserProfile;
$objlogin = new UserLogin();
$objpro = new UserProfile();
$objlogin->checkLogin();
$user_data = $objlogin->profile();
if (isset($_GET['id']) && $_SESSION['login_confirm']['is_admin']==1) {
    $objpro->prepare($_GET);
    $pro_data = $objpro->userProfile();
} else {
    $pro_data = $objpro->viewProfile();
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo $user_data['username'];?> | Profile</title>
<style>
    table {border-collapse:collapse;}
    td,th {padding: 5px;}
</style>
</head>
<body>
    <span>
        <a href="dashboard.php">Dashboard</a> 
        <a href="profile.php">Profile</a>
        <a href="edit.php">Edit Profile</a>
        <?php if ($_SESSION['login_confirm']['is_admin']==1) { ?><a href="user_manage.php">Manage User</a><?php } ?>
    </span>
    <span style="float: right;">Logged in As <a href="profile.php"><b><?php echo $_SESSION['login_confirm']['username'];?></b></a> 
        <a href="settings.php">Account Settings</a>
        <a href="logout.php">Logout</a></span>
    <hr/>
    <?php
    if (empty($pro_data['first_name']) && empty($pro_data['last_name']) && $_SESSION['login_confirm']['is_admin']==0) {
        echo 'Your profile is not Compleated. <a href="edit.php">Click Here</a> for update profile.';
    } elseif (empty($pro_data['first_name']) && empty($pro_data['last_name']) && $_SESSION['login_confirm']['is_admin']==1) {
        echo 'User profile is not Compleated. No Profile Data Availabale';
    }  else { ?>
    <table cellpadding="0" cellspacing="0" border="1" align="center">
        <tr>
            <td><img src="images/<?php echo $pro_data['image'];?>" width="200"/></td>
            <td></td>
        </tr>
        <tr>
            <th>Name</th>
            <td><?php echo $pro_data['first_name'].' '.$pro_data['last_name'];?></td>
        </tr>
        <tr>
            <th>Father Name</th>
            <td><?php echo $pro_data['father_name'] ?></td>
        </tr>
        <tr>
            <th>Mother Name</th>
            <td><?php echo $pro_data['mother_name'] ?></td>
        </tr>
        <tr>
            <th>Gender</th>
            <td><?php echo $pro_data['gender'] ?></td>
        </tr>
        <tr>
            <th>Date Of Birth</th>
            <td><?php echo $pro_data['birth_date'] ?></td>
        </tr>
    </table>
    <?php } ?>
    
</body>
</html>
