<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
</head>
<body>
<?php
session_start();
if (isset($_SESSION['error']) && !empty($_SESSION['error'])) {
    echo $_SESSION['error'];
    unset($_SESSION['error']);
}  else {
    header('location:index.php');
}
?>
</body>
</html>
