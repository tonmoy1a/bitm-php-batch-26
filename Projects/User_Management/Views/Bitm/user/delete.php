<?php
include '../../../vendor/autoload.php';
use RegistrationApp\Bitm\user_login\UserLogin;
use RegistrationApp\Bitm\user_profile\UserProfile;
$objlogin = new UserLogin();
$objpro = new UserProfile();
$objlogin->checkLogin();
if ($_SESSION['login_confirm']['is_admin']==1) {
    $objpro->prepare($_GET);
    $objpro->deleteUser();
}  else {
    header('location:error.php');
    $_SESSION['error'] = "Something Wrong.";
}
