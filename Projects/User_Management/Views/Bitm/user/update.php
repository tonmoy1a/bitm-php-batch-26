<?php

include '../../../vendor/autoload.php';

use RegistrationApp\Bitm\user_login\UserLogin;
use RegistrationApp\Bitm\user_profile\UserProfile;

$objlogin = new UserLogin();
$objpro = new UserProfile();

$objlogin->checkLogin();
$objpro->prepare($_POST);
$objpro->imageUpload();
$objpro->updateProfile();


//echo '<pre>';
//print_r($_POST);
?>
