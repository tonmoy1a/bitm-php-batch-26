<?php
include '../../../vendor/autoload.php';
use RegistrationApp\Bitm\user_login\UserLogin;
use RegistrationApp\Bitm\user_profile\UserProfile;
$objlogin = new UserLogin();
$objpro = new UserProfile();
$objlogin->checkLogin();
$user_data = $objlogin->profile();
$pro_data = $objpro->viewProfile();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo $_SESSION['login_confirm']['username'];?> | Dashboard</title>
</head>
<body>
    <span>
        <a href="dashboard.php">Dashboard</a> 
        <a href="profile.php">Profile</a>
        <a href="edit.php">Edit Profile</a>
        <?php if ($_SESSION['login_confirm']['is_admin']==1) { ?><a href="user_manage.php">Manage User</a><?php } ?>
    </span>
    <span style="float: right;">Logged in As <a href="profile.php"><b><?php echo $_SESSION['login_confirm']['username'];?></b></a> 
        <a href="settings.php">Account Settings</a>
        <a href="logout.php">Logout</a></span>
    <hr/>
    <h3>Welcome <?php echo $_SESSION['login_confirm']['username'];?></h3>
    <p>You are logged in.</p>
    <?php
    if (empty($pro_data['first_name']) && empty($pro_data['last_name'])) {
        echo 'Your profile is not Compleated. <a href="edit.php">Click Here</a> for update profile.';
    }
    ?>
</body>
</html>
