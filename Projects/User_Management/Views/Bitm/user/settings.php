<?php
include '../../../vendor/autoload.php';
use RegistrationApp\Bitm\user_login\UserLogin;
$objlogin = new UserLogin();
$objlogin->checkLogin();
$user_data = $objlogin->profile();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo $user_data['username'];?> | Edit Profile</title>
</head>
<body>
    <span>
        <a href="dashboard.php">Dashboard</a> 
        <a href="profile.php">Profile</a>
        <a href="edit.php">Edit Profile</a>
        <?php if ($_SESSION['login_confirm']['is_admin']==1) { ?><a href="user_manage.php">Manage User</a><?php } ?>
    </span>
    <span style="float: right;">Logged in As <a href="profile.php"><b><?php echo $user_data['username'];?></b></a> 
        <a href="settings.php">Account Settings</a>
        <a href="logout.php">Logout</a></span>
    <hr/>
    <?php $objlogin->sessionMessage('message');?>
    <form method="post" action="update.php">
        <table>
            <tr>
                <td><label>Old Password :</label></td>
                <td><input type="password" name="password_old"/></td>
                <td><?php $objlogin->sessionMessage('val_pass_old');$objlogin->sessionMessage('val_pass_req_old');?></td>
            </tr>
            <tr>
                <td><label>Password :</label></td>
                <td><input type="password" name="password"/></td>
                <td><?php $objlogin->sessionMessage('val_pass_lenth');$objlogin->sessionMessage('val_pass_req');?></td>
            </tr>
            <tr>
                <td><label>Retype Password :</label></td>
                <td><input type="password" name="password-retype"/></td>
                <td><?php $objlogin->sessionMessage('val_pass_match');$objlogin->sessionMessage('val_pass_retype');?></td>
            </tr>
            <tr>
                <td><input type="submit" value="Confirm"/> <input type="reset"/></td>
                <td></td>
            </tr>
        </table>
    </form>
</body>
</html>
