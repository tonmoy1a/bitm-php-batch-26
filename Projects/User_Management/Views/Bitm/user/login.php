<?php session_start(); 
if (!empty($_SESSION['login_confirm'])) {
    header('location:dashboard.php');
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login Account</title>
</head>
<body>
    <h3>Login</h3>
    <p>Not Registered User. <a href="index.php">Sign Up Now</a>.</p>
    <?php
    if (isset($_SESSION['confirm']) && !empty($_SESSION['confirm'])) {
        echo $_SESSION['confirm'];
        unset($_SESSION['confirm']);
    }
    ?>
    <form action="login_action.php" method="post">
        <table>
            <tr>
                <td><label>Username :</label></td>
                <td><input type="text" name="username"/></td>
            </tr>
            <tr>
                <td><label>Password :</label></td>
                <td><input type="password" name="password"/></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Login"/></td>
            </tr>
        </table>
    </form>
</body>
</html>