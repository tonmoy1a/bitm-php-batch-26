<?php
include '../../../vendor/autoload.php';
use RegistrationApp\Bitm\user_login\UserLogin;
use RegistrationApp\Bitm\user_profile\UserProfile;
$objlogin = new UserLogin();
$objpro = new UserProfile();
$objlogin->checkLogin();
$user_data = $objlogin->profile();
$pro_data = $objpro->viewProfile();
$address = unserialize($pro_data['address']);
$arr_hobbies = array('Writing', 'Reading', 'Music', 'Programming', 'Travel');
$arr_data = explode(', ', $pro_data['hobbies']);
$month_arr = array('January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
if (empty($pro_data['birth_date'])) { 
    $birth_date = array('','','');
}  else {
    $birth_date = explode('/', $pro_data['birth_date']) ;  
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo $user_data['username'];?> | Edit Profile</title>
<style>
    table {border: 1px black solid;padding: 5px;}
    .red {color: red}
</style>
</head>
<body>
    <span>
        <a href="dashboard.php">Dashboard</a> 
        <a href="profile.php">Profile</a>
        <a href="edit.php">Edit Profile</a>
        <?php if ($_SESSION['login_confirm']['is_admin']==1) { ?><a href="user_manage.php">Manage User</a><?php } ?>
    </span>
    <span style="float: right;">Logged in As <a href="profile.php"><b><?php echo $user_data['username'];?></b></a> 
        <a href="settings.php">Account Settings</a>
        <a href="logout.php">Logout</a></span>
    <hr/>
    <p style="text-align: center;"><?php $objlogin->sessionMessage('message');?></p>
    <form method="post" action="update.php" enctype="multipart/form-data">
        <table align="center">
            <tr>
                <td><label>First Name :</label></td>
                <td><input type="text" name="first_name" value="<?php
                    if (empty($pro_data['first_name'])) {
                        $objlogin->sessionMessage('v_firstname');
                    } else {
                        echo $pro_data['first_name'];
                    }
                    ?>" /></td>
                <td class="red">* <?php $objlogin->sessionMessage('First_Name');?></td>
            </tr>
            <tr>
                <td><label>Last Name :</label></td>
                <td><input type="text" name="last_name" value="<?php
                    if (empty($pro_data['last_name'])) {
                        $objlogin->sessionMessage('v_lastname');
                    } else {
                        echo $pro_data['last_name'];
                    }
                    ?>" /></td>
                <td class="red">* <?php $objlogin->sessionMessage('Last_Name');?></td>
            </tr>
            <tr>
                <td><label>Father's Name :</label></td>
                <td><input type="text" name="father_name" value="<?php
                    if (empty($pro_data['father_name'])) {
                        $objlogin->sessionMessage('v_fathername');
                    } else {
                        echo $pro_data['father_name'];
                    }
                    ?>" /></td>
                <td class="red">* <?php $objlogin->sessionMessage('Fathers_Name');?></td>
            </tr>
            <tr>
                <td><label>Mother's Name :</label></td>
                <td><input type="text" name="mother_name" value="<?php
                    if (empty($pro_data['mother_name'])) {
                        $objlogin->sessionMessage('v_mothername');
                    } else {
                        echo $pro_data['mother_name'];
                    }
                    ?>" /></td>
                <td class="red">* <?php $objlogin->sessionMessage('Mothers_Name');?></td>
            </tr>
            <tr>
                <td><label>Profile Image :</label></td>
                <td>
                    <img src="images/<?php if (empty($pro_data['image'])) {echo 'nobody.jpg';} else {echo $pro_data['image'];}?>" width="150"/><br/>
                    <input type="file" name="image"/>
                </td>
                <td class="red">
                    <?php $objlogin->sessionMessage('err_files_ext');?><br/>
                    <?php $objlogin->sessionMessage('err_files_size');?><br/>
                    <?php $objlogin->sessionMessage('req_image');?>
                </td>
            </tr>
            <tr>
                <td><label>Gender :</label></td>
                <td>
                    <input type="radio" name="gender" value="Male" <?php if ($pro_data['gender']=='Male') {echo 'checked';}?> /><label>Male</label>
                    <input type="radio" name="gender" value="Female" <?php if ($pro_data['gender']=='Female') {echo 'checked';}?>/><label>Female</label>
                    <input type="radio" name="gender" value="Others" <?php if ($pro_data['gender']=='Others') {echo 'checked';}?>/><label>Others</label>
                </td>
                <td class="red">* <?php $objlogin->sessionMessage('Gender');?></td>
            </tr>
            <tr>
                <td><label>Birth Date :</label></td>
                <td>
                    <select name="day">
                        <option value="">Day</option>
                        <?php for ($i=1;$i<=31;$i++) { ?>
                        <option value="<?php echo $i;?>" <?php if ($birth_date['0']==$i){echo 'selected';}?>><?php echo $i;?></option>
                        <?php } ?>
                    </select>
                    <select name="month">
                        <option value="">Month</option>
                        <?php foreach ($month_arr as $month) { ?>
                        <option value="<?php echo $month;?>" <?php if ($birth_date['1']==$month){echo 'selected';}?>><?php echo $month;?></option>
                        <?php } ?>
                    </select>
                    <select name="year">
                        <option value="">Year</option>
                        <?php for ($i=1969;$i<=2011;$i++) { ?>
                        <option value="<?php echo $i;?>" <?php if ($birth_date['2']==$i){echo 'selected';}?>><?php echo $i;?></option>
                        <?php } ?>
                    </select>
                </td>
                <td class="red">* <?php $objlogin->sessionMessage('Birth_Date');?></td>
            </tr>
            <tr>
                <td><label>Nationality :</label></td>
                <td><input type="text" name="nationality" value="<?php
                    if (empty($pro_data['nationality'])) {
                        $objlogin->sessionMessage('v_nationality');
                    } else {
                        echo $pro_data['nationality'];
                    }
                    ?>" /></td>
                <td class="red">* <?php $objlogin->sessionMessage('Nationality');?></td>
            </tr>
            <tr>
                <td><label>Religion :</label></td>
                <td><input type="text" name="religion" value="<?php
                    if (empty($pro_data['religion'])) {
                        $objlogin->sessionMessage('v_religion');
                    } else {
                        echo $pro_data['religion'];
                    }
                    ?>" /></td>
                <td></td>
            </tr>
            <tr>
                <td><label>Occupation :</label></td>
                <td><input type="text" name="occupation" value="<?php
                    if (empty($pro_data['occupation'])) {
                        $objlogin->sessionMessage('v_occupation');
                    } else {
                        echo $pro_data['occupation'];
                    }
                    ?>" /></td>
                <td></td>
            </tr>
            <tr>
                <td><label>Current Workplace :</label></td>
                <td><input type="text" name="workplace" value="<?php
                    if (empty($pro_data['workplace'])) {
                        $objlogin->sessionMessage('v_workplace');
                    } else {
                        echo $pro_data['workplace'];
                    }
                    ?>" /></td>
                <td></td>
            </tr>
            <tr>
                <td><label>Education :</label></td>
                <td><input type="text" name="education" value="<?php
                    if (empty($pro_data['education'])) {
                        $objlogin->sessionMessage('v_education');
                    } else {
                        echo $pro_data['education'];
                    }
                    ?>" /></td>
                <td class="red">* <?php $objlogin->sessionMessage('Education');?></td>
            </tr>
            <tr>
                <td><label>Marital Status :</label></td>
                <td>
                    <input type="radio" name="marital_status" value="Married" <?php if ($pro_data['marital_status']=='Married') {echo 'checked';}?> /><label>Married</label>
                    <input type="radio" name="marital_status" value="Divorced" <?php if ($pro_data['marital_status']=='Divorced') {echo 'checked';}?> /><label>Divorced</label>
                    <input type="radio" name="marital_status" value="Single" <?php if ($pro_data['marital_status']=='Single') {echo 'checked';}?> /><label>Single</label>
                </td>
                <td></td>
            </tr>
            <tr>
                <td><label>Blood Group :</label></td>
                <td>
                    <select name="blood_group">
                        <option value="">Select</option>
                        <option value="O+" <?php if($pro_data['blood_group']=='O+'){echo 'selected';}?>>O+</option>
                        <option value="O-" <?php if($pro_data['blood_group']=='O-'){echo 'selected';}?>>O-</option>
                        <option value="A+" <?php if($pro_data['blood_group']=='A+'){echo 'selected';}?>>A+</option>
                        <option value="A-" <?php if($pro_data['blood_group']=='A-'){echo 'selected';}?>>A-</option>
                        <option value="B+" <?php if($pro_data['blood_group']=='B+'){echo 'selected';}?>>B+</option>
                        <option value="B-" <?php if($pro_data['blood_group']=='B-'){echo 'selected';}?>>B-</option>
                        <option value="AB+" <?php if($pro_data['blood_group']=='AB+'){echo 'selected';}?>>AB+</option>
                        <option value="AB-" <?php if($pro_data['blood_group']=='AB-'){echo 'selected';}?>>AB-</option>
                    </select>
                </td>
                <td class="red"></td>
            </tr>
            <tr>
                <td><label>Language :</label></td>
                <td><input type="text" name="language" value="<?php echo $pro_data['language'];?><?php
                    if (empty($pro_data['language'])) {
                        $objlogin->sessionMessage('v_language');
                    } else {
                        echo $pro_data['language'];
                    }
                    ?>" /></td>
                <td></td>
            </tr>
            <tr>
                <td><label>Height :</label></td>
                <td><input type="number" name="height" value="<?php
                    if (empty($pro_data['height'])) {
                        $objlogin->sessionMessage('v_height');
                    } else {
                        echo $pro_data['height'];
                    }
                    ?>" /> <label>in Inch</label></td>
                <td></td>
            </tr>
            <tr>
                <td><label>Skills :</label></td>
                <td><input type="text" name="skills" value="<?php
                    if (empty($pro_data['skills'])) {
                        $objlogin->sessionMessage('v_skill');
                    } else {
                        echo $pro_data['skills'];
                    }
                    ?>" placeholder="Separated by Commas"/></td>
                <td></td>
            </tr>
            <tr>
                <td><label>Hobbies :</label></td>
                <td>
                    <?php foreach ($arr_hobbies as $single_hobbie) { ?>
                    <input type="checkbox" name="hobby[]" value="<?php echo $single_hobbie;?>" <?php 
                         foreach ($arr_data as $data) {
                             if ($single_hobbie == $data) {echo 'checked'; }
                         }
                    ?> /><label><?php echo $single_hobbie;?></label><br/>
                    <?php } ?>
                    
                </td>
                <td></td>
            </tr>
            <tr>
                <td><label>Biography :</label></td>
                <td><textarea name="biography"><?php
                    if (empty($pro_data['biography'])) {
                        $objlogin->sessionMessage('v_bio');
                    } else {
                        echo $pro_data['biography'];
                    }
                    ?></textarea></td>
                <td></td>
            </tr>
            <tr>
                <td><label>Address :</label></td>
                <td>
                    <input type="text" name="add_line1" value="<?php echo $address[0];?>" placeholder="Line 1" />
                </td>
                <td class="red">* <?php $objlogin->sessionMessage('Address');?></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="text" name="add_line2" value="<?php echo $address[1];?>" placeholder="Line 2"/>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="text" name="add_city" value="<?php echo $address[2];?>" placeholder="City"/>
                </td>
                <td class="red">*</td>
            </tr>
            <tr>
                <td><label>Mobile :</label></td>
                <td><input type="number" name="mobile" value="<?php
                    if (empty($pro_data['mobile'])) {
                        $objlogin->sessionMessage('v_mobile');
                    } else {
                        echo $pro_data['mobile'];
                    }
                    ?>" /></td>
                <td class="red">* <?php $objlogin->sessionMessage('Mobile');?></td>
            </tr>
            <tr>
                <td><label>Web Address :</label></td>
                <td><input type="text" name="web_address" value="<?php
                    if (empty($pro_data['web'])) {
                        $objlogin->sessionMessage('v_web');
                    } else {
                        echo $pro_data['web'];
                    }
                    ?>" /></td>
                <td></td>
            </tr>
            <tr>
                <td><label>Fax :</label></td>
                <td><input type="text" name="fax" value="<?php
                    if (empty($pro_data['fax'])) {
                        $objlogin->sessionMessage('v_fax');
                    } else {
                        echo $pro_data['fax'];
                    }
                    ?>" /></td>
                <td></td>
            </tr>
            <tr>
                <td><label>National Id No :</label></td>
                <td><input type="text" name="nation_id" value="<?php
                    if (empty($pro_data['national_id'])) {
                        $objlogin->sessionMessage('v_nationalid');
                    } else {
                        echo $pro_data['national_id'];
                    }
                    ?>" /></td>
                <td></td>
            </tr>
            <tr>
                <td><label>Passport :</label></td>
                <td><input type="text" name="passport" value="<?php
                    if (empty($pro_data['passport'])) {
                        $objlogin->sessionMessage('v_passport');
                    } else {
                        echo $pro_data['passport'];
                    }
                    ?>" /></td>
                <td></td>
            </tr>
            <tr>
                <td><input type="hidden" name="image" value="<?php echo $pro_data['image'];?>"/></td>
                <td><input type="submit" value="Confirm"/> <input type="reset"/></td>
            </tr>
        </table>
    </form>
</body>
</html>
