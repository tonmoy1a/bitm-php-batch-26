<?php

include '../../../vendor/autoload.php';

use RegistrationApp\Bitm\user_login\UserLogin;

$objlogin = new UserLogin();
$objlogin->checkLogin();
$user_data = $objlogin->profile();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $error = FALSE;
    if (!empty($_POST['password_old'])) {
        if ($user_data['password'] == $_POST['password_old']) {
            
        } else {
            $_SESSION['val_pass_old'] = 'Old Password does not Match.<br/>';
            $error = true;
        }
    } else {
        $_SESSION['val_pass_req_old'] = 'Enter Old Password.<br/>';
        $error = true;
    }

    if (!empty($_POST['password'])) {
        if (strlen($_POST['password']) >= 6 && strlen($_POST['password']) <= 12) {
            
        } else {
            $_SESSION['val_pass_lenth'] = 'Password Must Be Within 6 to 12 Character.<br/>';
            $error = true;
        }
    } else {
        $_SESSION['val_pass_req'] = 'Enter New Password.<br/>';
        $error = true;
    }

    if (!empty($_POST['password-retype'])) {
        if ($_POST['password'] !== $_POST['password-retype']) {
            $_SESSION['val_pass_match'] = 'New Password does not Matched.<br/>';
            $error = true;
        }
    } else {
        $_SESSION['val_pass_retype'] = 'Retype Password.<br/>';
        $error = true;
    }
    
    if ($error==FALSE) {
        $objlogin->prepare($_POST);
        $objlogin->update();
        $_SESSION['message'] = "Password Updated Successfully.";
    }  else {
        
    }
    header('location:edit.php');
} else {
    $_SESSION['error'] = "Opps something going wrong!";
    header('location:error.php');
}
?>
