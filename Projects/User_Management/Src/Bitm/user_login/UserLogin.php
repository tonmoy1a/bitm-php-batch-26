<?php

namespace RegistrationApp\Bitm\user_login;

use PDO;

class UserLogin {

    public $id, $uid, $uname, $upass, $data, $viewBy, $error, $conn;

    public function __construct() {
        if (empty(session_id())) {
            session_start();
        }
        $this->conn = new PDO('mysql:host=localhost;dbname=tproject', 'root', '');
    }

    public function prepare($data = '') {
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (!empty($data['username'])) {
            $this->uname = $data['username'];
        }
        if (!empty($data['password'])) {
            $this->upass = $data['password'];
        }
        if (!empty($data['uid'])) {
            $this->uid = $data['uid'];
        }
        if (!empty($data['view_by'])) {
            $this->viewBy = $data['view_by'];
        }
    }

    public function sessionMessage($val_sess = '') {
        if (!empty($_SESSION["$val_sess"]) && isset($_SESSION["$val_sess"])) {
            echo $_SESSION["$val_sess"];
            unset($_SESSION["$val_sess"]);
        }
    }

    public function loginConfirm() {
        $query = $this->conn->query("SELECT * FROM users WHERE username='$this->uname' AND '$this->upass' ");
        $row = $query->fetch();
        if (!empty($row['username'])) {
            if ($row['deleted'] == 0) {
                $_SESSION['login_confirm'] = $row;
                header('location:dashboard.php');
            }  else {
                $_SESSION['confirm'] = "Your Accound Suspended.";
                header('location:login.php');
            }
        } else {
            $_SESSION['confirm'] = "Username and Password does not Match.";
            header('location:login.php');
        }
    }

    public function checkLogin() {
        if (empty($_SESSION['login_confirm'])) {
            header('location:login.php');
        }
    }

    public function profile() {
        $this->id = $_SESSION['login_confirm']['id'];
        $query = $this->conn->query("SELECT * FROM users WHERE id='$this->id' ");
        $row = $query->fetch();
        return $row;
    }

    public function update() {
        $this->id = $_SESSION['login_confirm']['id'];
        try {
            $update_query = "UPDATE `users` SET `password` = :password , `is_admin` = :is_admin , `modified` = :modify WHERE `users`.`id` = :id ;";
            $stmt = $this->conn->prepare($update_query);
            $stmt->execute(array(
                ':password' => $this->upass,
                ':is_admin' => 1,
                ':modify' => date("Y-m-d h:i:s"),
                ':id' => $this->id
            ));
//            $_SESSION['message'] = "Data Updated Successfully.";
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessege();
        }
    }

    public function userData() {
        try {
            if (empty($this->viewBy)) {
                $vq = "SELECT * FROM `users` WHERE deleted=0 ";
            } else {
                $vq = "SELECT * FROM `users` WHERE deleted=1 ";
            }

            $view_query = $this->conn->prepare($vq);
            $view_query->execute();
            while ($row = $view_query->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
            return $this->data;
        } catch (Exception $ex) {
            
        }
    }

}

?>