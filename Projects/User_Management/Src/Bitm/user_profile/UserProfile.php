<?php

namespace RegistrationApp\Bitm\user_profile;

use PDO;

class UserProfile {

    public $id, $userid, $firstName, $lastName, $fatherName, $motherName, $image, $birthDate, $gender, $address,
            $nationality, $religion, $occupation, $workplace, $education, $maritalStatus, $bloodGroup, $language,
            $height, $skill, $hobby, $biography, $mobile, $web, $fax, $nationId, $passport, $error;

    public function __construct() {
        if (empty(session_id())) {
            session_start();
        }
        $this->conn = new PDO('mysql:host=localhost;dbname=tproject', 'root', '');
    }

    public function prepare($data = '') {
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (!empty($data['username'])) {
            $this->userid = $data['username'];
        }
        if (!empty($data['first_name'])) {
            $this->firstName = $data['first_name'];
        }
        if (!empty($data['last_name'])) {
            $this->lastName = $data['last_name'];
        }
        if (!empty($data['father_name'])) {
            $this->fatherName = $data['father_name'];
        }
        if (!empty($data['mother_name'])) {
            $this->motherName = $data['mother_name'];
        }
        if (!empty($data['image'])) {
            $this->image = $data['image'];
        }
        if (!empty($data['day']) && !empty($data['month']) && !empty($data['year'])) {
            $this->birthDate = $data['day'] . '/' . $data['month'] . '/' . $data['year'];
        }
        if (!empty($data['gender'])) {
            $this->gender = $data['gender'];
        }
        if (!empty($data['nationality'])) {
            $this->nationality = $data['nationality'];
        }
        if (!empty($data['religion'])) {
            $this->religion = $data['religion'];
        }
        if (!empty($data['occupation'])) {
            $this->occupation = $data['occupation'];
        }
        if (!empty($data['workplace'])) {
            $this->workplace = $data['workplace'];
        }
        if (!empty($data['education'])) {
            $this->education = $data['education'];
        }
        if (!empty($data['marital_status'])) {
            $this->maritalStatus = $data['marital_status'];
        }
        if (!empty($data['blood_group'])) {
            $this->bloodGroup = $data['blood_group'];
        }
        if (!empty($data['language'])) {
            $this->language = $data['language'];
        }
        if (!empty($data['height'])) {
            $this->height = $data['height'];
        }
        if (!empty($data['skills'])) {
            $this->skill = $data['skills'];
        }
        if (!empty($data['hobby'])) {
            $this->hobby[] = $data['hobby'];
        }
        if (!empty($data['biography'])) {
            $this->biography = $data['biography'];
        }
        if (!empty($data['add_line1']) && !empty($data['add_city'])) {
            $this->address = serialize(array($data['add_line1'], $data['add_line2'], $data['add_city']));
        }
        if (!empty($data['mobile'])) {
            $this->mobile = $data['mobile'];
        }
        if (!empty($data['web_address'])) {
            $this->web = $data['web_address'];
        }
        if (!empty($data['fax'])) {
            $this->fax = $data['fax'];
        }
        if (!empty($data['nation_id'])) {
            $this->nationId = $data['nation_id'];
        }
        if (!empty($data['passport'])) {
            $this->passport = $data['passport'];
        }
    }

    public function imageUpload() {
        if (empty($this->image)) {
            if (isset($_FILES['image']) && !empty($_FILES['image']['name'])) {
                $this->image = time() . '_' . $_FILES['image']['name'];
                $files_type = $_FILES['image']['type'];
                $files_tmp_name = $_FILES['image']['tmp_name'];
                $files_size = $_FILES['image']['size'];
                $files_extension = strtolower(end(explode('.', $this->image)));

                $req_extension = array('jpg', 'jpeg', 'png', 'gif');
                $error = '';
                if (in_array($files_extension, $req_extension) === FALSE) {
                    $_SESSION['err_files_ext'] = 'jpg, jpeg, png, gif required.';
                    $error = TRUE;
                }

                if ($files_size > 2097152) {
                    $_SESSION['err_files_size'] = 'File Size Must Be Under 2 MB.';
                    $error = TRUE;
                }

                if (empty($error)) {
                    move_uploaded_file($files_tmp_name, 'images/' . $this->image);
                }
            } else {
                $_SESSION['req_image'] = 'Select Your Profile Image.';
                $this->error = TRUE;
            }
        } else {
            if (isset($_FILES['image']) && !empty($_FILES['image']['name'])) {
                $this->image = time() . '_' . $_FILES['image']['name'];
                $files_type = $_FILES['image']['type'];
                $files_tmp_name = $_FILES['image']['tmp_name'];
                $files_size = $_FILES['image']['size'];
                $files_extension = strtolower(end(explode('.', $this->image)));

                $req_extension = array('jpg', 'jpeg', 'png', 'gif');
                $error = '';
                if (in_array($files_extension, $req_extension) === FALSE) {
                    $_SESSION['err_files_ext'] = 'jpg, jpeg, png, gif required.';
                    $error = TRUE;
                }

                if ($files_size > 2097152) {
                    $_SESSION['err_files_size'] = 'File Size Must Be Under 2 MB.';
                    $error = TRUE;
                }

                if (empty($error)) {
                    move_uploaded_file($files_tmp_name, 'images/' . $this->image);
                } else {
                    $this->error = TRUE;
                }
            }
        }
    }

    public function updateProfile() {
        $required = array(
            'First_Name' => $this->firstName,
            'Last_Name' => $this->lastName,
            'Fathers_Name' => $this->fatherName,
            'Mothers_Name' => $this->motherName,
            'Gender' => $this->gender,
            'Birth_Date' => $this->birthDate,
            'Nationality' => $this->nationality,
            'Education' => $this->education,
            'Address' => $this->address,
            'Mobile' => $this->mobile,
        );
        foreach ($required as $key => $field) {
            if (empty($field)) {
                $_SESSION["$key"] = '<i>(Required)</i>';
                $this->error = TRUE;
            }
        }
        if ($this->error == FALSE) {
            try {
                $hobbies = implode($this->hobby[0], ', ');
                $update_query = "UPDATE `profile` SET `first_name` = :firstname, `last_name` = :lastname, `father_name` = :fathername, `mother_name` = :mothername, `image` = :image, `gender` = :gender, `birth_date` = :birthdate, `nationality` = :nationality, `religion` = :religion, `occupation` = :occupation, `workplace` = :workplace, `education` = :education, `marital_status` = :maritalstatus, `blood_group` = :bloodgroup, `language` = :language, `height` = :height, `skills` = :skill, `hobbies` = :hobbie, `biography` = :bio, `address` = :address, `mobile` = :mobile, `web` = :web, `fax` = :fax, `national_id` = :nationalid, `passport` = :passport, `ooo` = :ooo WHERE `profile`.`user_id` = :userId";
                $stmt = $this->conn->prepare($update_query);
                $stmt->execute(array(
                    ':firstname' => $this->firstName,
                    ':lastname' => $this->lastName,
                    ':fathername' => $this->fatherName,
                    ':mothername' => $this->motherName,
                    ':image' => $this->image,
                    ':gender' => $this->gender,
                    ':birthdate' => $this->birthDate,
                    ':nationality' => $this->nationality,
                    ':religion' => $this->religion,
                    ':occupation' => $this->occupation,
                    ':workplace' => $this->workplace,
                    ':education' => $this->education,
                    ':maritalstatus' => $this->maritalStatus,
                    ':bloodgroup' => $this->bloodGroup,
                    ':language' => $this->language,
                    ':height' => $this->height,
                    ':skill' => $this->skill,
                    ':hobbie' => $hobbies,
                    ':bio' => $this->biography,
                    ':address' => $this->address,
                    ':mobile' => $this->mobile,
                    ':web' => $this->web,
                    ':fax' => $this->fax,
                    ':nationalid' => $this->nationId,
                    ':passport' => $this->passport,
                    ':ooo' => '0',
                    ':userId' => $_SESSION['login_confirm']['id'],
                ));
                $_SESSION['message'] = "Data Updated Successfully.";
                header('location:edit.php');
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessege();
            }
        } else {
            $_SESSION['v_firstname'] = $this->firstName;
            $_SESSION['v_lastname'] = $this->lastName;
            $_SESSION['v_fathername'] = $this->fatherName;
            $_SESSION['v_mothername'] = $this->motherName;
            $_SESSION['v_gender'] = $this->gender;
            $_SESSION['v_birthdate'] = $this->birthDate;
            $_SESSION['v_nationality'] = $this->nationality;
            $_SESSION['v_religion'] = $this->religion;
            $_SESSION['v_occupation'] = $this->occupation;
            $_SESSION['v_workplace'] = $this->workplace;
            $_SESSION['v_education'] = $this->education;
            $_SESSION['v_maritalstatus'] = $this->maritalStatus;
            $_SESSION['v_bloodgroup'] = $this->bloodGroup;
            $_SESSION['v_language'] = $this->language;
            $_SESSION['v_height'] = $this->height;
            $_SESSION['v_skill'] = $this->skill;
            $_SESSION['v_hobbie'] = $this->hobby;
            $_SESSION['v_bio'] = $this->biography;
            $_SESSION['v_address'] = $this->address;
            $_SESSION['v_mobile'] = $this->mobile;
            $_SESSION['v_web'] = $this->web;
            $_SESSION['v_fax'] = $this->fax;
            $_SESSION['v_nationalid'] = $this->nationId;
            $_SESSION['v_passport'] = $this->passport;
        }
        header('location:edit.php');
    }

    public function viewProfile() {
        $this->userid = $_SESSION['login_confirm']['id'];
        $query = $this->conn->query("SELECT * FROM profile WHERE user_id='$this->userid' ");
        $row = $query->fetch();
        return $row;
    }

    public function userProfile() {
        $query = $this->conn->query("SELECT * FROM profile WHERE user_id='$this->id' ");
        $row = $query->fetch();
        return $row;
    }

    public function deleteUser() {
        $query = $this->conn->prepare("UPDATE `users` SET `deleted` = '1' WHERE `users`.`id` = '" . $this->id . "'");
        $query->execute();
        header('location:user_manage.php');
    }

}
