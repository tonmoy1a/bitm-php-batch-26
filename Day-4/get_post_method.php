<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Simple Application</title>
    </head>
    <body>
        <h3>Form Data Submit Practice and Lab</h3>
        <form method="POST" action="viewProfile.php">
            <table>
                <tr>
                    <td>Name:</td>
                    <td><input type="text" name="name"/></td>
                </tr>
                <tr>
                    <td>Age:</td>
                    <td><input type="number" name="age"/></td>
                </tr>
                <tr>
                    <td>Gender:</td>
                    <td><input type="radio" name="gender" value="Male" checked=""/> Male <input type="radio" name="gender" value="Female"/> Female </td>
                </tr>
                <tr>
                    <td>Phone:</td>
                    <td><input type="number" name="phone"/></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><input type="text" name="email"/></td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td><textarea name="address"></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit"/></td>
                </tr>
            </table>
        </form>
    </body>
</html>
