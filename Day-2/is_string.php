<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>is_string</title>
    </head>
    <body>
        <h3>is_string — Find whether the type of a variable is string</h3>
        <?php
            $var1 = "PHP";
            echo is_string($var1) ? "$var1 is string<br/>" : "$var1 is not string<br/>";
            
            $var2 = 1;
            echo is_string($var2) ? "$var2 is string<br/>" : "$var2 is not string<br/>";
            
            $var3 = "PHP";
            if (is_string($var3)) {
                echo "is_string return true for $var3<br/>";
            }  else {
                echo "is_string return false for $var3<br/>";
            }
            
            $var4 = 5;
            if (is_string($var4)) {
                echo "is_string return true for $var4<br/>";
            }  else {
                echo "is_string return false for $var4<br/>";
            }
        ?>
    </body>
</html>
