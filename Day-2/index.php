<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Day 2 - PHP Batch 26</title>
    </head>
    <body>
        <?php
            echo 'Hello World';
        ?>
        <h2>PHP Variable Handling Functions</h2>
        <ul>
            <li><a href="boolval.php">boolval</a></li>
            <li><a href="empty.php">empty</a></li>
            <li><a href="gettype.php">gettype</a></li>
            <li><a href="is_array.php">is_array</a></li>
            <li><a href="is_int.php">is_int</a></li>
            <li><a href="is_null.php">is_null</a></li>
            <li><a href="is_object.php">is_object</a></li>
            <li><a href="is_string.php">is_string</a></li>
            <li><a href="isset.php">isset</a></li>
            <li><a href="print_r.php">print_r</a></li>
            <li><a href="unserialize.php">unserialize</a></li>
            <li><a href="unset.php">unset</a></li>
            <li><a href="var_dump.php">var_dump</a></li>
        </ul>
    </body>
</html>