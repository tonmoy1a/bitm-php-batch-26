<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Is_Array</title>
    </head>
    <body>
        <h3>is_array — Finds whether a variable is an array</h3>
        <?php
            $yes = array(1,2,3,4,5);
            echo is_array($yes) ? 'Array<br/>' : 'not an Array<br/>';
            
            $no = 1;
            echo is_array($no) ? 'Array<br/>':'not an Array<br/>';
        
            $array = array(1,2,3,4,5);
            if (is_array($array)) {
                echo 'is_array return true for array(1,2,3,4,5)<br/>';
            }  else {
                
            }
            
            $not_array = 5;
            if (is_array($not_array)) {
                echo 'is_array return true for array(1,2,3,4,5)<br/>';
            }  else {
                echo 'is_array return false for integer<br/>';
            }
            
            $not_array = "PHP";
            if (is_array($not_array)) {
                echo 'is_array return true for array(1,2,3,4,5)<br/>';
            }  else {
                echo 'is_array return false for String<br/>';
            }
            
        ?>
    </body>
</html>
