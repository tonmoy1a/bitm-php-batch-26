<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>is_object</title>
    </head>
    <body>
        <h3>is_object — Finds whether a variable is an object</h3>
        <?php
            function get_student($obj){
                if(!is_object($obj))
                {
                    return false;
                }
                return $obj->students;
            }
            
            $obj = new stdClass();
            $obj->students = array('abc', 'def', 'ghi');
            
            var_dump(get_student(null));
            var_dump(get_student($obj));
        ?>
    </body>
</html>
