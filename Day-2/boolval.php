<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Boolval</title>
    </head>
    <body>
        <h3>boolval — Get the boolean value of a variable</h3>
        <p>Example</p>
        <?php
            echo '0 : '.(boolval(0)? 'true' : 'false').'</br>';
            echo '40 : '.(boolval(40)? 'true' : 'false').'</br>';
            echo '0.0 : '.(boolval(0.0)? 'true' : 'false').'</br>';
            echo '0.1 : '.(boolval(0.1)? 'true' : 'false').'</br>';
            echo '"" : '.(boolval("")? 'true' : 'false').'</br>';
            echo 'String : '.(boolval("String")? 'true' : 'false').'</br>';
            echo '"0" : '.(boolval("0")? 'true' : 'false').'</br>';
            echo '"1" : '.(boolval("1")? 'true' : 'false').'</br>';
            echo '[1, 2, 3, 4] : '.(boolval([1, 2, 3, 4])? 'true' : 'false').'</br>';
            echo '[] : '.(boolval([])? 'true' : 'false').'</br>';
            echo 'stdClass : '.(boolval(new stdClass)? 'true' : 'false').'</br>';
        ?>
    </body>
</html>
