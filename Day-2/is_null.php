<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>is_null</title>
    </head>
    <body>
        <h3>is_null — Finds whether a variable is NULL</h3>
        <?php
            $var1 = null;
            echo is_null($var1) ? "$var1 is null<br/>" : "$var1 is not null<br/>";
            
            $var2 = 2;
            echo is_null($var2) ? "$var2 is null<br/>" : "$var2 is not null<br/>";
            
            $var3 = null;
            if (is_null($var3)) {
                echo "is_null return true for $var3<br/>";
            }  else {
                echo "is_int return false for $var3<br/>";
            }
            
            $var4 = "PHP";
            if (is_null($var4)) {
                echo "is_int return true for $var4<br/>";
            }  else {
                echo "is_int return false for $var4<br/>";
            }
        ?>
    </body>
</html>
