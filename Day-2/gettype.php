<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Gettype</title>
    </head>
    <body>
        <h3>gettype — Get the type of a variable</h3>
        <?php
            $var1 = 1;
            $var2 = 1.2;
            $var3 = true;
            $var4 = null;
            $var5 = "PHP";
            $var6 = new stdClass();
            $data = array(1,2,3,4);
            echo "$var1 is ".gettype($var1).'</br>';
            echo "$var2 is ".gettype($var2).'</br>';
            echo "$var3 is ".gettype($var3).'</br>';
            echo "$var4 is ".gettype($var4).'</br>';
            echo "$var5 is ".gettype($var5).'</br>';
            echo "stdClass is ".gettype($var6).'</br>';
            echo "array is ".gettype($data).'</br>';
        ?>
    </body>
</html>
