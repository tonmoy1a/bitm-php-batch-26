<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>var_dump</title>
    </head>
    <body>
        <h3>var_dump — Dumps information about a variable</h3>
        <?php
            $var1 = array(1, 2,3 , array("a", "b", "c"));
            $var2 = 111;
            $var3 = 'ABC';
            $var4 = true;
            $var5 = null;
            $var6 = 1.2;
            var_dump($var1);echo '<br/>';
            var_dump($var2);echo '<br/>';
            var_dump($var3);echo '<br/>';
            var_dump($var4);echo '<br/>';
            var_dump($var5);echo '<br/>';
            var_dump($var6);echo '<br/>';
        ?>
    </body>
</html>
