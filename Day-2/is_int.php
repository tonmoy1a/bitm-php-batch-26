<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>is_int</title>
    </head>
    <body>
        <h3>is_int — Find whether the type of a variable is integer</h3>
        <?php
            $var1 = 1;
            echo is_int($var1) ? "$var1 is integer<br/>" : "$var1 is not integer<br/>";
            
            $var2 = "PHP";
            echo is_int($var2) ? "$var2 is integer<br/>" : "$var2 is not integer<br/>";
            
            $var3 = 5;
            if (is_int($var3)) {
                echo "is_int return true for $var3<br/>";
            }  else {
                echo "is_int return false for $var3<br/>";
            }
            
            $var4 = "PHP";
            if (is_int($var4)) {
                echo "is_int return true for $var4<br/>";
            }  else {
                echo "is_int return false for $var4<br/>";
            }
        ?>
    </body>
</html>
