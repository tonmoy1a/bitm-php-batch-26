<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>unset</title>
    </head>
    <body>
        <h3>unset — Unset a given variable</h3>
        <?php
            $var1 = array(0 => "a", 1 => "b", 2 => "c");
            unset($var1[1]);
            print_r($var1);
            
            echo '<br/>';
            
            $var2 = "PHP";
            $var3 = "JAVA";
            unset($var3);
            echo $var2.'<br/>';
            echo isset($var3) ? "$var3":"variable unset";
        ?>
    </body>
</html>
