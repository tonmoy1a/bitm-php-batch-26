<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>unserialize</title>
    </head>
    <body>
        <h3>unserialize — Creates a PHP value from a stored representation </h3>
        <?php
            $arr = array('First Name', 'Tonmoy', );
            $serial = serialize($arr);
            print_r($serial);
            echo '<br/>';
            $unserial = unserialize($serial);
            print_r($unserial);
        ?>
    </body>
</html>
