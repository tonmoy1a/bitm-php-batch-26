<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Empty</title>
    </head>
    <body>
        <h3>empty — Determine whether a variable is empty</h3>
        <?php
            $var1 = 0;
            if (empty($var)) {
                echo "Empty $var1 return true.</br>";
            }
            
            $var2 = 1;
            if (empty($var)) {
                echo "Empty $var2 return true.</br>";
            }
            else {
                echo "Empty $var2 return false.</br>";
            }
            
            $var3 = "";
            if (empty($var)) {
                echo "Empty String $var3 return true.</br>";
            }
            else {
                echo "Empty String $var3 return false.</br>";
            }
            
            $var4 = '"PHP"';
            if (empty($var)) {
                echo "Empty String $var4 return true.";
            }
            else {
                echo "Empty String $var4 return false.";
            }
        ?>
    </body>
</html>
