<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>print_r </title>
    </head>
    <body>
        <h3>print_r — Prints human-readable information about a variable</h3>
        <?php
            $var1 = array('a', 'b', 'c', 'd', 'e');
            print_r($var1);
            echo '<br/>';
            $var2 = array('a'=>'Apple', 'b'=>'Banana', 'c'=> array('x','y','z'));
            print_r($var2);
        ?>
    </body>
</html>
