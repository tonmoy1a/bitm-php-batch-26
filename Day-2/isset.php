<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>isset</title>
    </head>
    <body>
        <h3>isset — Determine if a variable is set and is not NULL</h3>
        <?php
            $var1 = '';
            if (isset($var1)) {
                echo 'This variable is set<br/>';
            }
            
            $var2 = null;
            if (isset($var2)) {
                echo 'This variable is set<br/>';
            } else {
                echo 'This variable is not set for null';
            }
        ?>
    </body>
</html>
