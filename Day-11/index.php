<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h3>Inheritance</h3>
        <ul>
            <li><a href="public.php">Public</a></li>
            <li><a href="protected.php">Protected</a></li>
            <li><a href="private.php">Private</a></li>
        </ul>
    </body>
</html>
