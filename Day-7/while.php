<h3>PHP while loops execute a block of code while the specified condition is true.</h3>
<?php
$x = 1; 

while($x <= 5) {
    echo "The number is: $x <br>";
    $x++;
} 
?>
