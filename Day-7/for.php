<h3>The for loop is used when you know in advance how many times the script should run.</h3>
<?php
for ($x = 0; $x <= 10; $x++) {
    echo "The number is: $x <br>";
} 
?>
