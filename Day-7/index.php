<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h3>PHP Control Structure</h3>
        <ul>
            <li><a href="ifelse.php">if Else</a></li>
            <li><a href="elseif.php">else if</a></li>
            <li><a href="while.php">while</a></li>
            <li><a href="dowhile.php">do while</a></li>
            <li><a href="for.php">for</a></li>
            <li><a href="foreach.php">foreach</a></li>
            <li><a href="break.php">break</a></li>
            <li><a href="continue.php">continue</a></li>
            <li><a href="switch.php">switch</a></li>
            <li><a href="declare.php">declare</a></li>
            <li><a href="return.php">return</a></li>
            <li><a href="require.php">require</a></li>
            <li><a href="include.php">include</a></li>
            <li><a href="require_once.php">require_once</a></li>
            <li><a href="include_once.php">include_once</a></li>
            <li><a href="goto.php">goto</a></li>
        </ul>
    </body>
</html>
