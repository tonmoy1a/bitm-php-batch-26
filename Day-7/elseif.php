<h3>The if....elseif...else statement executes different codes for more than two conditions.</h3>
<?php
$var = 40;

if ($var > 33 && $var <=40) {
    echo "D Grade";
} elseif ($var > 41 && $var <=50) {
    echo "C grede";
} 
elseif ($var > 51 && $var <=60) {
    echo "Have a good day!";
}
elseif ($var > 61 && $var <=70) {
    echo "B Grade";
}
elseif ($var > 71 && $var <=80) {
    echo "A Grade";
}

else {
    echo "A+";
}
?>
