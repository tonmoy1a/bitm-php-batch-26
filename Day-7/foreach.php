<h3>The foreach loop works only on arrays, and is used to loop through each key/value pair in an array.</h3>
<?php
$colors = array("red", "green", "blue", "yellow"); 

foreach ($colors as $value) {
    echo "$value <br>";
}
?>
