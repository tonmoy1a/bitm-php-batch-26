<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>PHP Batch 26</title>
</head>
<body>
    <h2>Class Practice</h2>
    <ul>
        <li><a href="CRUD/">CRUD</a></li>
	<li><a href="Projects/">Projects</a></li>
        <li><a href="Day-2/">Day 2</a> - <i>19/05/16</i></li>
        <li><a href="Day-3/">Day 3</a> - <i>22/05/16</i></li>
        <li><a href="Day-4/">Day 4</a> - <i>24/05/16</i></li>
        <li><a href="Day-5/">Day 5</a> - <i>26/05/16</i></li>
        <li><a href="Day-6/">Day 6</a> - <i>29/05/16</i></li>
        <li><a href="Day-7/">Day 7</a> - <i>31/05/16</i></li>
        <li><a href="Day-9/">Day 9</a> - <i>07/05/16</i></li>
        <li><a href="Day-10/">Day 10</a> - <i>09/06/16</i></li>
        <li><a href="Day-11/">Day 11</a> - <i>12/06/16</i></li>
    </ul>
</body>
</html>
