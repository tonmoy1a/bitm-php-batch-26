<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Get and Post Method</title>
    </head>
    <body>
        <h3>Get Method Form</h3>
        <form method="get">
        <table>
            <tr>
                <td>Name</td>
                <td><input type="text" name="name"/></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password"/></td>
            </tr>
            <tr>
                <td>Address</td>
                <td><textarea></textarea></td>
            </tr>
            <tr>
                <td>Select Subject:</td>
                <td>
                    <input type="checkbox" name="subject" value="Bangla"/>Bangla
                    <input type="checkbox" name="subject" value="English"/>English
                    <input type="checkbox" name="subject" value="History"/>History
                </td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>
                    Male <input type="radio" name="gender" value="male"/>
                    Female<input type="radio" name="gender" value="female"/>
                </td>
            </tr>
            <tr>
                <td>Select Package</td>
                <td>
                    <select name="package">
                        <option>2000 BDT</option>
                        <option>5000 BDT</option>
                        <option>10000 BDT</option>
                        <option>20000 BDT</option>
                        <option>25000 BDT</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit"/></td>
            </tr>
        </table>
        </form>
        
        <h3>Post Method Form</h3>
        <form method="post">
        <table>
            <tr>
                <td>Name</td>
                <td><input type="text" name="name"/></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password"/></td>
            </tr>
            <tr>
                <td>Address</td>
                <td><textarea></textarea></td>
            </tr>
            <tr>
                <td>Select Subject:</td>
                <td>
                    <input type="checkbox" name="subject" value="Bangla"/>Bangla
                    <input type="checkbox" name="subject" value="English"/>English
                    <input type="checkbox" name="subject" value="History"/>History
                </td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>
                    Male <input type="radio" name="gender" value="male"/>
                    Female<input type="radio" name="gender" value="female"/>
                </td>
            </tr>
            <tr>
                <td>Select Package</td>
                <td>
                    <select name="package">
                        <option>2000 BDT</option>
                        <option>5000 BDT</option>
                        <option>10000 BDT</option>
                        <option>20000 BDT</option>
                        <option>25000 BDT</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit"/></td>
            </tr>
        </table>
        </form>
    </body>
</html>
