<?php
include_once 'calculator.php';
$getresult = new Calculator();
$getresult->calculate();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Simple Calculator</title>
    </head>
    <body>
        <form method="get">
            <input type="number" name="num1" placeholder="Enter First Number" value="<?php echo $getresult->result;?>"/>
            <select name="oparator">
                <option>+</option>
                <option>-</option>
                <option>*</option>
                <option>/</option>
            </select>
            <input type="number" name="num2" placeholder="Enter Second Number"/>
            <input type="submit" value="Calculate"/>
        </form>
        <?php
            echo $getresult->calculate();
        ?>
    </body>
</html>
