<?php

class Calculator {

    public $number1, $number2, $oparator, $result, $value;

    public function calculate() {
        if (isset($_GET['num1'])) {
            $this->number1 = $_GET['num1'];
            $this->number2 = $_GET['num2'];
            $this->oparator = $_GET['oparator'];

            if ($this->oparator == '+') {
                $this->result = ($this->number1 + $this->number2);
            } elseif ($this->oparator == '-') {
                $this->result = ($this->number1 - $this->number2);
            } elseif ($this->oparator == '*') {
                $this->result = ($this->number1 * $this->number2);
            } elseif ($this->oparator == '/') {
                $this->result = ($this->number1 / $this->number2);
            }
            return "Result: <b>" . $this->result."</b>";
        } else {
            return "Please Enter Two numbers.";
        }
    }

}

?>
