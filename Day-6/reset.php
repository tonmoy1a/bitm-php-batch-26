<h3>Output the value of the current and next element in an array, then reset the array's internal pointer to the first element in the array</h3>
<?php
$arr = array("apple","banana","orrange","pineapple","mango");
next($arr);
echo current($arr);
echo '<br/>';
echo reset($arr);
?>
