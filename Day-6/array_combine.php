<h3>Creates an array by using one array for keys and another for its values</h3>
<pre>
<?php
$fname = array("Peter","Ben","Joe");
$age = array("35","37","43");

$c=array_combine($fname,$age);
print_r($c);


$a = array('green', 'red', 'yellow');
$b = array('avocado', 'apple', 'banana');
$c = array_combine($a, $b);

print_r($c);
?>

