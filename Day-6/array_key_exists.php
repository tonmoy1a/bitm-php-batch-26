<h3>Checks if the given key or index exists in the array</h3>
<?php
$arr = array("Volvo"=>"XC90","BMW"=>"X5");
if (array_key_exists("Volvo",$arr))
  {
  echo "Key exists!";
  }
else
  {
  echo "Key does not exist!";
  }
?>