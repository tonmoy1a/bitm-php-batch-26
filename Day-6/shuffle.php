<h3>Randomize the order of the elements in the array</h3>
<pre>
<?php
$arr = array("apple","banana","orrange","pineapple","mango");
shuffle($arr);
print_r($arr);

$arr = array("a"=>"apple","b"=>"banana","c"=>"orrange","d"=>"pineapple","e"=>"mango");
shuffle($arr);
print_r($arr);
?>
