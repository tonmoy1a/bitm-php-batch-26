<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Array Function</title>
    </head>
    <body>
        <h3>PHP Array Function (Home Task)</h3>
        <ul>
            <li><a href="array_combine.php">array_combine</a></li>
            <li><a href="array_filter.php">array_filter</a></li>
            <li><a href="array_key_exists.php">array_key_exists</a></li>
            <li><a href="array_keys.php">array_keys</a></li>
            <li><a href="array_pad.php">array_pad</a></li>
            <li><a href="array_merge.php">array_merge</a></li>
            <li><a href="array_pop.php">array_pop</a></li>
            <li><a href="array_push.php">array_push</a></li>
            <li><a href="array_rand.php">array_rand</a></li>
            <li><a href="array_replace.php">array_replace, array_replace_recursive, array_search</a></li>
            <li><a href="array_reverse.php">array_reverse</a></li>
            <li><a href="array_shift.php">array_shift</a></li>
            <li><a href="array_sum.php">array_sum</a></li>
            <li><a href="array_unique.php">array_unique</a></li>
            <li><a href="array_unshift.php">array_unshift</a></li>
            <li><a href="array_values.php">array_values</a></li>
            <li><a href="array_walk.php">array_walk</a></li>
            <li><a href="asort.php">asort</a></li>
            <li><a href="compact.php">compact</a></li>
            <li><a href="count.php">count</a></li>
            <li><a href="current.php">current</a></li>
            <li><a href="each.php">each</a></li>
            <li><a href="end.php">end</a></li>
            <li><a href="in_array.php">in_array</a></li>
            <li><a href="key.php">key</a></li>
            <li><a href="list.php">list</a></li>
            <li><a href="next.php">next</a></li>
            <li><a href="pos.php">pos</a></li>
            <li><a href="prev.php">prev</a></li>
            <li><a href="reset.php">reset</a></li>
            <li><a href="shuffle.php">shuffle</a></li>
            <li><a href="sizeof.php">sizeof</a></li>
            <li><a href="sort.php">sort</a></li>
        </ul>
    </body>
</html>
