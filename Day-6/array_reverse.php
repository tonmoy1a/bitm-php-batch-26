<h3>Return an array in the reverse order</h3>
<?php
echo '<pre>';
$arr = array("apple","banana","orrange","pineapple","mango");
print_r(array_reverse($arr));

echo '<br/>';

$a=array("apple","banana",array("orrange","pineapple"));
$reverse=array_reverse($a);
$preserve=array_reverse($a,true);

print_r($a);
print_r($reverse);
print_r($preserve);
?>