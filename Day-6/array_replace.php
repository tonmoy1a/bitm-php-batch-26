<h3>Replace the values of the first array ($a1) with the values from the second array ($a2)</h3>
<?php
echo '<pre>';
$a1=array("apple","banana");
$a2=array("orrange","pineapple");
print_r(array_replace($a1,$a2));

echo '<br/>';

$a1=array("apple","banana");
$a2=array("orrange","pineapple");
$a3=array("mango","bean");
print_r(array_replace($a1,$a2,$a3));

echo '<br/>';

$a1=array("a"=>array("apple"),"b"=>array("banana","orrange"),);
$a2=array("a"=>array("pineapple"),"b"=>array("bean"));
print_r(array_replace_recursive($a1,$a2));

echo '<br/>';

$a=array("a"=>"red","b"=>"green","c"=>"blue");
echo array_search("red",$a);

echo '<br/>';

$a=array("a"=>"5","b"=>5,"c"=>"5");
echo array_search(5,$a,true);
?>
