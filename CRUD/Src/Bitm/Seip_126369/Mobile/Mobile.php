<?php

namespace MobileApp\Bitm\Seip_126369\Mobile;

use PDO;

class Mobile {

    public $id, $title, $laptop, $data, $uid, $conn, $dbuser = 'root', $dbpass = '';

    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:host=localhost;dbname=tproject', $this->dbuser, $this->dbpass);
//        try {
//	    $this->conn = new PDO('mysql:host=localhost;dbname=tproject', $this->dbuser, $this->dbpass);
//	    $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//	} catch (PDOException $e) {
//	    echo 'ERROR: ' . $e->getMessage();
//	}
    }

    public function prepare($data = '') {
        if (!empty($data['mobile_model'])) {
            $this->title = $data['mobile_model'];
        }
        if (!empty($data['laptop'])) {
            $this->laptop = $data['laptop'];
        }
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (!empty($data['uid'])) {
            $this->uid = $data['uid'];
        }

        return $this;
    }

    public function store() {
        if (!empty($this->title) && !empty($this->laptop)) {
            try {
                $query = "INSERT INTO mobiles(id, mobile, uid, laptop) VALUES (:i, :m, :u, :l)";
                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':i' => null,
                    ':m' => $this->title,
                    ':u' => uniqid(),
                    ':l' => $this->laptop
                ));
                $_SESSION['message'] = "Data Submitted Successfully.";
            } catch (PDOException $e) {
                echo 'Error: '. $e->getMessage();
            }
        } else {
            $_SESSION['title_value'] = "$this->title";
            $_SESSION['laptop_value'] = "$this->laptop";
            if (empty($this->title)) {
                $_SESSION['req_name'] = "Mobile Name Required.";
            }
            if (empty($this->laptop)) {
                $_SESSION['req_laptop'] = "Laptop Name Required.";
            }
        }
        header('location:create.php');
    }

    public function validationMessage($val_sess = '') {
        if (!empty($_SESSION["$val_sess"]) && isset($_SESSION["$val_sess"])) {
            echo $_SESSION["$val_sess"];
            unset($_SESSION["$val_sess"]);
        }
    }

    public function index() {
        $vq = "SELECT * FROM `mobiles`";
        $view_query = $this->conn->prepare($vq);
        $view_query->execute();
        while ($row = $view_query->fetch(PDO::FETCH_ASSOC)) {
            $this->data[] = $row;
        }
        return $this->data;
    }

    public function show() {
        $vqs = "SELECT * FROM `mobiles` WHERE uid='" . $this->uid . "'";
	$view_data_single = $this->conn->prepare($vqs);
	$view_data_single->execute();
	$row = $view_data_single->fetch(PDO::FETCH_ASSOC);
	return $row;
    }

    public function update() {
        if (!empty($this->title) && !empty($this->laptop)) {
            try {
                $update_query = "UPDATE mobiles SET mobile = :m, laptop = :l WHERE mobiles.uid = :u ";
                $stmt = $this->conn->prepare($update_query);
                $stmt->execute(array(
                    ':m'=> $this->title,
                    ':l'=> $this->laptop,
                    ':u'=> $this->uid
                ));
                $_SESSION['message'] = "Data Updated Successfully.";
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessege();
            }
        } else {
            if (empty($this->title)) {
                $_SESSION['req_name_u'] = "Mobile Name Required.";
            }
            if (empty($this->laptop)) {
                $_SESSION['req_laptop_u'] = "Laptop Name Required.";
            }
        }
        header("location:edit.php?uid=$this->uid");
    }

    public function delete() {
        try {
	    $delete_query = "DELETE FROM `mobiles` WHERE `mobiles`.`uid` = :u ";
	    $stmt = $this->conn->prepare($delete_query);
	    $stmt->execute(array(
		':u' => $this->uid
	    ));
            $_SESSION['message'] = "<h4>Data deleted successfully.</h4>";
	} catch (PDOException $e) {
	    echo 'Error: ' . $e->getMessage();
	}
        header("location:index.php");
    }

}

?>