<?php

namespace HobbyApp\Bitm\Seip_126369\Hobby;

class Hobby {

    public $id, $name, $hobby, $data, $uid;

    public function __construct() {
        session_start();
        mysql_connect('localhost', 'root', '') or die('Database Connection Error.');
        mysql_select_db('tproject') or die('Database seletion Error');
    }

    public function prepare($data = '') {
        if (!empty($data['name'])) {
            $this->name = $data['name'];
        } else {
            $_SESSION['req_name'] = "Name Required";
        }
        if (!empty($data['hobby'])) {
            $this->hobby[] = $data['hobby'];
        } else {
            $_SESSION['req_hobby'] = "Select Minimum One Hobby";
        }
        if (!empty($data['uid'])) {
            $this->uid = $data['uid'];
        }

        return $this;
    }

    public function sessionMessege($session_key = '') {
        if (isset($_SESSION["$session_key"]) && !empty("$session_key")) {
            echo $_SESSION["$session_key"];
            unset($_SESSION["$session_key"]);
        }
    }

    public function store() {
        if (!empty($this->name) && !empty($this->hobby)) {
            $hobbies = implode($this->hobby[0], ', ');
            $query = "INSERT INTO `hobbies` (`id`, `name`, `hobbie`, `uid`) VALUES (NULL, '$this->name', '$hobbies', '" . uniqid() . "')";
            if (mysql_query($query)) {
                $_SESSION['message'] = "Data Submitted Successfully.";
            }
        } else {
            
        }
        header('location:create.php');
    }

    public function index() {
        $query_index = "SELECT * FROM `hobbies` ";
        $query_data = mysql_query($query_index);
        while ($row = mysql_fetch_assoc($query_data)) {
            $this->data[] = $row;
        }
        return $this->data;
    }

    public function view() {
        $query_view = "SELECT * FROM `hobbies` WHERE `uid`= '" . $this->uid . "' ";
        $view_data = mysql_query($query_view);
        $row = mysql_fetch_assoc($view_data);
        return $row;
    }

    public function update() {
        if (!empty($this->name) && !empty($this->hobby)) {
            $hobbies = implode($this->hobby[0], ', ');
            $query_update = "UPDATE `hobbies` SET `name` = '$this->name', `hobbie` = '$hobbies' WHERE `hobbies`.`uid` = '" . $this->uid . "'";
            if (mysql_query($query_update)) {
                $_SESSION['message'] = "Data Updated Successfully.";
            }
        }  else {
            if (empty($this->name)) {
                $_SESSION['req_name_u'] = "Name Required.";
            }
            if (empty($this->hobby)) {
                $_SESSION['req_hob_u'] = "Select Minimum One Hobby.";
            }
        }
        header("location:edit.php?uid=$this->uid");
    }

}
