<?php include_once '../../../../Src/Bitm/Seip-126369/Mobile/Mobile.php'; 
$updatewobj = new Mobile();
$onedata = $updatewobj->prepare($_GET);
$singledata = $updatewobj->show();

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mobile Model | Edit</title>
    </head>
    <body>
        <br/><a href="index.php">View all mobile data</a>
        <fieldset>
            <legend>Edit Mobile Mobile Mobile Model Data</legend>
            <form method="post" action="update.php">
            <input type="text" name="mobile_model" value="<?php echo $singledata['mobile'] ;?>"/>
            <input type="hidden" name="id" value="<?php echo $_GET['id'] ;?>"/>
            <input type="submit" value="Edit"/>
        </form>
        </fieldset>
    </body>
</html>
