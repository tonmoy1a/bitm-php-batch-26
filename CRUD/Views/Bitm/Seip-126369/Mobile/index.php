<?php include_once '../../../../Src/Bitm/Seip-126369/Mobile/Mobile.php'; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mobile Model</title>
    </head>
    <body>
        <a href="../../../../index.php">List of all project's</a>
        <h2>Mobile Model</h2>
        <ul>
            <li><a href="create.php">Add New Mobile</a></li>
        </ul>
        <?php
        $mobileobj = new Mobile();
        $alldata = $mobileobj->index();

        if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
        ?>
        <table border="1">
            <?php 
            if (isset($alldata) && !empty($alldata)) {
            ?>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th colspan="3">Action</th>
            </tr>
            <?php
                foreach ($alldata as $singledata) {
                    ?>
                    <tr>
                        <td><?php echo $singledata['id']; ?></td>
                        <td><?php echo $singledata['mobile']; ?></td>
                        <td><a href="view.php?id=<?php echo $singledata['id']; ?>">View</a></td>
                        <td><a href="edit.php?id=<?php echo $singledata['id']; ?>">Edit</a></td>
                        <td><a href="delete.php?id=<?php echo $singledata['id']; ?>">Delete</a></td>
                    </tr>
            <?php
                }
            } else {
                echo 'Sorry no data available.<br/>Insert Some Data.';
}
            ?>
        </table>
    </body>
</html>
