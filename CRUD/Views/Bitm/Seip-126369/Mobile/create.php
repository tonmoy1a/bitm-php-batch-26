<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mobile Model | Add Mobile</title>
    </head>
    <body>
        <a href="index.php">Go to main page</a><br/>
        <?php
        if (isset($_SESSION['message'])) {
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
        ?>
        <fieldset>
            <legend>Add Favorite Mobile Model</legend>
            <form action="store.php" method="post">
                <input type="text" name="mobile_model"/>
                <input type="submit" value="Submit"/>
            </form>
        </fieldset>
    </body>
</html>