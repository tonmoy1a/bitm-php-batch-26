<?php
include '../../../../vendor/autoload.php';

use HobbyApp\Bitm\Seip_126369\Hobby\Hobby;

$obj = new Hobby();
$alldata = $obj->index();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Your Hobbies</title>
</head>
<body>
        <h3>Hobby List With Name</h3>
        <a href="create.php">Add Hobbies</a>
<?php if (isset($alldata) && !empty($alldata)) { ?>
            <table border="1">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Hobbies</th>
                    <th colspan="3">Action</th>
                </tr>
    <?php foreach ($alldata as $singledata) { ?>
                    <tr>
                        <td><?php echo $singledata['id']; ?></td>
                        <td><?php echo $singledata['name']; ?></td>
                        <td><?php echo $singledata['hobbie']; ?></td>
                        <td><a href="view.php?uid=<?php echo $singledata['uid']; ?>">View</a></td>
                        <td><a href="edit.php?uid=<?php echo $singledata['uid']; ?>">Edit</a></td>
                        <td><a href="delete.php?uid=<?php echo $singledata['uid']; ?>">Delete</a></td>
                    </tr>
            <?php } ?>
            </table>
        <?php
        } else {
            echo 'No Data Available.<br/>Please Insert Some data.';
        }
        ?>
</body>
</html>
