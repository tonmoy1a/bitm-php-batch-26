<?php
include '../../../../vendor/autoload.php';
use HobbyApp\Bitm\Seip_126369\Hobby\Hobby;
$obj = new Hobby();
$onedata = $obj->prepare($_GET)->view();
if (isset($onedata) && !empty($onedata)) {
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>View Your Hobby</title>
    </head>
    <body>
        <a href="index.php">View All List data</a>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Hobbies</th>
            </tr>
            <tr>
                <td><?php echo $onedata['id']; ?></td>
                <td><?php echo $onedata['name']; ?></td>
                <td><?php echo $onedata['hobbie']; ?></td>
            </tr>
        </table>
    </body>
</html>
<?php }
 else {
     $_SESSION['err_msg'] = "Error Data.";
     header('location:error.php');
}
?>
