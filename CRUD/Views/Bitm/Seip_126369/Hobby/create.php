<?php
include '../../../../vendor/autoload.php';
use HobbyApp\Bitm\Seip_126369\Hobby\Hobby;
$obj = new Hobby();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Add Your Hobbies</title>
    </head>
    <body>
        <fieldset>
            <legend>Add Name and Hobbies</legend>
            <font color="green"><?php $obj->sessionMessege('message')?></font>
            <form method="post" action="store.php">
                <label>Name </label><input type="text" name="name"/> <font color="red"><?php $obj->sessionMessege('req_name')?></font><br/>
                <label>Select Hobbies:</label>
                <font color="red"><?php $obj->sessionMessege('req_hobby')?></font><br/>
                <input type="checkbox" name="hobby[]" value="Writing" /><label>Writing</label><br/>
                <input type="checkbox" name="hobby[]" value="Reading" /><label>Reading</label><br/>
                <input type="checkbox" name="hobby[]" value="Music" /><label>Music</label><br/>
                <input type="checkbox" name="hobby[]" value="Programming" /><label>Programming</label><br/>
                <input type="checkbox" name="hobby[]" value="Travel" /><label>Travel</label><br/><br/>
                <input type="submit" value="Add Hobbies"/>
            </form>
        </fieldset>
    </body>
</html>
