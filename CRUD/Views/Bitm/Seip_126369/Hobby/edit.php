<?php
include '../../../../vendor/autoload.php';

use HobbyApp\Bitm\Seip_126369\Hobby\Hobby;

$obj = new Hobby();
$obj->prepare($_GET);
$onedata = $obj->view();
$arr_hobbies = array('Writing', 'Reading', 'Music', 'Programming', 'Travel');
$arr_data = explode(', ', $onedata['hobbie']);
if (isset($onedata) && !empty($onedata)) {
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>Add Your Hobbies</title>
        </head>
        <body>
            <fieldset>
                <legend>Add Name and Hobbies</legend>
                <font color="green"><?php $obj->sessionMessege('message') ?></font>
                <form method="post" action="update.php">
                    <label>Name </label><input type="text" name="name" value="<?php echo $onedata['name']; ?>"/><font color="red"><?php $obj->sessionMessege('req_name_u') ?></font><br/>
                    <label>Select Hobbies:</label><br/><font color="red"><?php $obj->sessionMessege('req_hob_u'); ?></font><br/>
                    <?php foreach ($arr_hobbies as $hobbies) { ?>
                        <input type="checkbox" name="hobby[]" value="<?php echo $hobbies; ?>" <?php
                        foreach ($arr_data as $data) {
                            if ($hobbies == $data) {
                                echo 'checked';
                            }
                        }
                        ?> /><label><?php echo $hobbies; ?></label><br/>
    <?php } ?>
                    <input type="hidden" name="uid" value="<?php echo $onedata['uid']; ?>"/>
                    <input type="submit" value="Add Hobbies"/>
                </form>
            </fieldset>
        </body>
    </html>
    <?php
} else {
    $_SESSION['err_msg'] = "Error Data.";
    header('location:error.php');
}
?>