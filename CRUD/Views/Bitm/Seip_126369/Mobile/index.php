<?php include_once '../../../../vendor/autoload.php'; 
use MobileApp\Bitm\Seip_126369\Mobile\Mobile;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mobile Model</title>
    </head>
    <body>
        <a href="../../../../index.php">List of all project's</a>
        <h2>Mobile Model</h2>
        <ul>
            <li><a href="create.php">Add New Data</a> | Download as <a href="pdf.php">Pdf</a> <a href="xl.php">XL</a> | <a href="phpmailer.php">Send Email</a></li>
        </ul>
        <?php
        $mobileobj = new Mobile();
        $alldata = $mobileobj->index();
        $mobileobj->validationMessage('message');
        ?>
        <table border="1">
            <?php 
            if (isset($alldata) && !empty($alldata)) {
            ?>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Laptop</th>
                <th colspan="3">Action</th>
            </tr>
            <?php
                foreach ($alldata as $singledata) {
                    ?>
                    <tr>
                        <td><?php echo $singledata['id']; ?></td>
                        <td><?php echo $singledata['mobile']; ?></td>
                        <td><?php echo $singledata['laptop'] ;?></td>
                        <td><a href="view.php?uid=<?php echo $singledata['uid']; ?>">View</a></td>
                        <td><a href="edit.php?uid=<?php echo $singledata['uid']; ?>">Edit</a></td>
                        <td><a href="delete.php?uid=<?php echo $singledata['uid']; ?>">Delete</a></td>
                    </tr>
            <?php
                }
            } else {
                echo 'Sorry no data available.<br/>Insert Some Data.';
}
            ?>
        </table>
    </body>
</html>
