<?php include_once '../../../../vendor/autoload.php'; 
use MobileApp\Bitm\Seip_126369\Mobile\Mobile;
$updatewobj = new Mobile();
$onedata = $updatewobj->prepare($_GET);
$singledata = $updatewobj->show();

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mobile Model | Edit</title>
    </head>
    <body>
        <a href="index.php">View all mobile data</a><br/>
        <?php $updatewobj->validationMessage('message'); ?>
        <fieldset>
            <legend>Edit Mobile Mobile Mobile Model Data</legend>
            <form method="post" action="update.php">
            <label>Mobile Model </label><input type="text" name="mobile_model" value="<?php echo $singledata['mobile'] ;?>"/><?php $updatewobj->validationMessage('req_name_u'); ?><br/>
            <label>Laptop Model </label><input type="text" name="laptop" value="<?php echo $singledata['laptop'] ;?>"/><?php $updatewobj->validationMessage('req_laptop_u'); ?><br/>
            <input type="hidden" name="uid" value="<?php echo $_GET['uid'] ;?>"/>
            <input type="submit" value="Edit"/>
        </form>
        </fieldset>
    </body>
</html>
