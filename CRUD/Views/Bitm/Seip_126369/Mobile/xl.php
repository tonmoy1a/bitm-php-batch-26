<?php
error_reporting(E_ALL);
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set("display_errors", TRUE);
ini_set("display_startup_errors", TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
    die('This example should only be run from a web browser.');

require_once '../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once '../../../../vendor/autoload.php';
use MobileApp\Bitm\Seip_126369\Mobile\Mobile;
$obj = new Mobile();
$alldata = $obj->index();

$objExal = new PHPExcel();
$objExal->getProperties()->setCreator('Tonmoy')
        ->setLastModifiedBy('Tonmoy')
        ->setTitle('Mobile And Laptop List')
        ->setSubject('Mobile And Laptop List')
        ->setDescription('Favorite Mobile And Laptop Data')
        ->setKeywords('office 2007')
        ->setCategory('Report file');

$objExal->setActiveSheetIndex(0)
        ->setCellValue('A1', 'SL')
        ->setCellValue('B1', 'Mobile')
        ->setCellValue('C1', 'Laptop');
$counter = 2;
$serial = 0;

foreach ($alldata as $data) {
    $serial++;
    $objExal->setActiveSheetIndex(0)
            ->setCellValue('A'.$counter , $serial)
            ->setCellValue('B'.$counter, $data['mobile'])
            ->setCellValue('C'.$counter, $data['laptop']);
    $counter++;
}

$objExal->getActiveSheet()->setTitle('Mobile_list');
$objExal->setActiveSheetIndex(0);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Mobile and Laptop Data.xls"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 23 Jul 1997 05:00:00 GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');

$objWriter = PHPExcel_IOFactory::createWriter($objExal, 'Excel5');
$objWriter->save('php://output');
exit();
?>
