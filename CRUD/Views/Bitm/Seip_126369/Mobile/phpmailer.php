<?php
require '../../../../vendor/phpmailer/phpmailer/class.phpmailer.php';
include '../../../../vendor/phpmailer/phpmailer/class.smtp.php';
include_once '../../../../vendor/autoload.php';
use MobileApp\Bitm\Seip_126369\Mobile\Mobile;

$obj = new Mobile();
$alldata = $obj->index();
$trs = "";
$serial = 0;

foreach ($alldata as $data):
    $serial++;
    $trs.="<tr>";
    $trs.="<td>" . $serial . "</td>";
    $trs.="<td>" . $data['mobile'] . "</td>";
    $trs.="<td>" . $data['laptop'] . "</td>";
    $trs.="</tr>";
    echo $trs;
endforeach;

$html = <<<EOD
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>List of Mobiles</h1>
        <table>
            <thead>
                <tr>
                    <th>Serial</th>
                    <th>Mobile</th>
                    <th>Laptop</th>
                </tr>
            </thead>
            <tbody>
                $trs;
            </tbody>
        </table>
    </body>
</html>
EOD;

$mail = new PHPMailer();
$mail = new PHPMailer;

$mail->isSMTP();
$mail->SMTPDebug = 2;
$mail->Debugoutput = 'html';
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
$mail->SMTPSecure = 'tls';
$mail->SMTPAuth = TRUE;
$mail->Username = '';
$mail->Password = '';
$mail->setFrom('', 'First Last');
$mail->addReplyTo('', 'First Last');
$mail->addAddress('', 'First Last');
$mail->Subject = 'subject';
$mail->Body = $html;
$mail->AltBody = 'This is a plain text message.';

if (!$mail->send()) {
    echo "mail Error".$mail->ErrorInfo;
} else {
    echo 'mail sent.';
}

?>
