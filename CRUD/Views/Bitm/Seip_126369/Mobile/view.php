<?php include_once '../../../../vendor/autoload.php';
use MobileApp\Bitm\Seip_126369\Mobile\Mobile;
$viewobj = new Mobile();
$onedata = $viewobj->prepare($_GET)->show();
if (isset($onedata) && !empty($onedata)) {
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mobile Models|Single View</title>
    </head>
    <body>
        <h2>Single View</h2>
        <a href="index.php">View List Page</a>
        <table border="1">
            <tr>
                <th>Id</th>
                <th>Mobile</th>
                <th>Laptop</th>
            </tr>
            <tr>
                <td><?php echo $onedata['id'];?></td>
                <td><?php echo $onedata['mobile'];?></td>
                <td><?php echo $onedata['laptop'];?></td>
            </tr>
        </table>
    </body>
</html>
<?php
}else {
    $_SESSION['err_msg'] = "Data error";
    header('location:error.php');
}
?>
