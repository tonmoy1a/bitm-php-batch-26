<?php
include_once '../../../../vendor/autoload.php';
$obj = new MobileApp\Bitm\Seip_126369\Mobile\Mobile();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mobile Model | Add Mobile</title>
    </head>
    <body>
        <a href="index.php">Go to main page</a><br/>
        <?php $obj->validationMessage('message'); ?>
        <fieldset>
            <legend>Add Favorite Mobile Model</legend>
            <form action="store.php" method="post">
                <label>Mobile Model </label><input type="text" name="mobile_model" value="<?php $obj->validationMessage('title_value'); ?>"><?php $obj->validationMessage('req_name'); ?><br/>
                <label>Laptop Model </label><input type="text" name="laptop" value="<?php $obj->validationMessage('laptop_value'); ?>"><?php $obj->validationMessage('req_laptop'); ?><br/>
                <input type="submit" value="Submit"/>
            </form>
        </fieldset>
    </body>
</html>