<?php

include '../../../../vendor/mpdf/mpdf/mpdf.php';
include_once '../../../../vendor/autoload.php';

use MobileApp\Bitm\Seip_126369\Mobile\Mobile;

$obj = new Mobile();
$alldata = $obj->index();
$trs = "";
$serial = 0;
foreach ($alldata as $data):
    $serial++;
    $trs.="<tr>";
    $trs.="<td>" . $serial . "</td>";
    $trs.="<td>" . $data['mobile'] . "</td>";
    $trs.="<td>" . $data['laptop'] . "</td>";
    $trs.="</tr>";
    echo $trs;
endforeach;
$html = <<<EOD
<!DOCTYPE html>
<html>
<head>
<title>Data</title>
</head>
<body>
        <h1>List of data</h1>
        <table border="1">
        <thead>
        <tr>
        <th>Serial</th>
        <th>Mobile</th>
        <th>Laptop</th>
        </tr>
        </thead>
        
        <tbody>
        $trs;
        </tbody>
        
        </table>

</body>
</html>
EOD;

$pdfobj = new mPDF();
$pdfobj->WriteHTML($html);
$pdfobj->Output();
exit();

?>

