<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Session Crud</title>
    </head>
    <body>
        <h3>CRUD using Session</h3>
        <a href="create.php">Add New Data</a><br/>
        <?php
        if (isset($_SESSION['ALL_DATA']) && !empty($_SESSION['ALL_DATA'])) {
            ?>
            <table border="1">
                <tr>
                    <th>ID</th>
                    <th>Mobile</th>
                    <th>Laptop</th>
                    <th colspan="3">Action</th>
                </tr>
                <?php foreach ($_SESSION['ALL_DATA'] as $key => $singledata) { ?>
                    <tr>
                        <td><?php echo $key; ?></td>
                        <td><?php echo $singledata['mobile_model']; ?></td>
                        <td><?php echo $singledata['laptop']; ?></td>
                        <td><a href="view.php?id=<?php echo $key; ?>">View</a></td>
                        <td><a href="edit.php?id=<?php echo $key; ?>">Edit</a></td>
                        <td><a href="delete.php?id=<?php echo $key; ?>">Delete</a></td>
                    </tr>
                <?php } ?>
            </table>
        <?php
        } else {
            echo 'No data available<br/>Insert some data.';
        }
        ?>
    </body>
</html>
